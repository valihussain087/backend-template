// load aws sdk
const debug = require("debug")("nlearn");
const aws = require("aws-sdk");
const awsConfig = require("../config/awsConfig.json");

// load aws config
aws.config.update(awsConfig);
// load AWS SES
const ses = new aws.SES();

const sendMail = (email, subject, output)=>
	new Promise((resolve, reject) => {
		ses.sendEmail({
			ConfigurationSetName: "MCEN",
			Source: "gcf.nlpl.dev@gmail.com",
			Destination: { ToAddresses: [email] },
			Message: {
				Subject: {
					Data: subject
				},
				Body: {
					Html: {
						Data: output
					}
				}
			}
		}, (err, data) => {
			if (err) reject(err);
			resolve(data);
		});
	});
const sendEmailToSingleUser = (msgData)=> new Promise((resolve, reject)=> {
	sendMail(msgData.email, msgData.message, msgData.output)
		.then((result)=> resolve(result))
		.catch((err)=> reject(err));
});

module.exports = {sendEmailToSingleUser};