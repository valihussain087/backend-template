const { getResponseObject } = require("../../helpers/supporter");

module.exports.registerUserParams = ()=>
	[
		{ type: "string", value: "state" },
		{ type: "string", value: "district" },
		{ type: "string", value: "district" }

	];

module.exports.registerUser = async (req, res, next)=> {
	const response = getResponseObject();

	return res.status(200).json(response);
};