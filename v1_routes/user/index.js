const express = require("express");

const router = express.Router();
const paramValidator = require("../../middlewares/paramsValidator");
const { catchErrors } = require("../../middlewares/errorHandlers");
const controller = require("../../controllers/user/");


//register user
router.post("/register", paramValidator(controller.registerUserParams()), catchErrors(controller.updateCollege));


module.exports = router;