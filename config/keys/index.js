const appRoot = require("app-root-path");
const fs = require("fs");

const privateKeyPath = `${appRoot}/config/keys/nlearn-private.pem`;
const publicKeyPath = `${appRoot}/config/keys/nlearn-public.pem`;

const privateCert = fs.readFileSync(privateKeyPath);
const publicCert = fs.readFileSync(publicKeyPath);
const jwtValidatityKey = "nlearn-jwt-validatity";

module.exports = {
	privateCert,
	publicCert,
	jwtValidatityKey,
	JwtIssuer: "jwt-nlearn",
	RefreshTokenIssuer: "refresh-token-nlearn"
};
